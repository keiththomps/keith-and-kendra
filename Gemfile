source 'https://rubygems.org'
ruby '2.0.0'

gem 'rails', '4.0.0'

gem 'puma'
gem 'pg', group: [:production]
gem 'sqlite3', group: [:development, :test]
gem 'rails_12factor', group: :production

gem 'sass-rails', '~> 4.0.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'bourbon'
gem 'neat'
gem 'jquery-rails'

group :development do
  gem 'bullet'
  gem 'flay'
  gem 'flog'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'lol_dba'
  gem 'quiet_assets'
  gem 'request-log-analyzer'
  gem 'rails_best_practices'
  gem 'meta_request'
end

group :development, :test do
  gem 'pry'
  gem 'debugger'
  gem 'jasmine-rails'

  # Live reloading of browser when working with views
  gem 'rb-fsevent'
  gem 'guard-livereload'
end

group :test do
  gem 'mocha', require: false
  gem 'database_cleaner'
  gem 'capybara_minitest_spec'
  gem 'minitest-matchers'
  gem 'poltergeist'
  gem 'cucumber-rails', require: false
  gem 'email_spec'
end
