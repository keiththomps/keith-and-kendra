App.controller 'RsvpCtrl', ['$scope', 'Rsvp', ($scope, Rsvp) ->
  $scope.guestNumbers = [1..7]
  $scope.porkOptions = [0..1]
  $scope.chickenOptions = [0..1]
  $scope.hideForm = false

  $scope.newRsvp = {
    name: ''
    attending: 'true'
    guests: 1
    pork_orders: 0
    chicken_orders: 0
  }

  # Handle recalculation of options
  $scope.calculatePorkOptions = ->
    max = $scope.newRsvp.guests - $scope.newRsvp.chicken_orders
    $scope.porkOptions = [0..max]

  $scope.calculateChickenOptions = ->
    max = $scope.newRsvp.guests - $scope.newRsvp.pork_orders
    $scope.chickenOptions = [0..max]

  $scope.calculateOptions = ->
    $scope.calculatePorkOptions()
    $scope.calculateChickenOptions()

  # Validation
  numbersAddUp = ->
    $scope.newRsvp.guests is $scope.newRsvp.pork_orders + $scope.newRsvp.chicken_orders

  $scope.invalid = ->
    return true if $scope.newRsvp.name is ''
    return false if $scope.newRsvp.attending is 'false'
    !numbersAddUp()

  # Send off form to create Rsvp
  $scope.addRsvp = ->
    Rsvp.save($scope.newRsvp)
    $scope.hideForm = true
]
