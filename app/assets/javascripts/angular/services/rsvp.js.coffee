App.factory 'Rsvp', ['$resource', ($resource) ->
  $resource '/api/rsvps/:id', { id: '@id' }
]
