class Api::RsvpsController < ApplicationController
  protect_from_forgery with: :null_session
  respond_to :json

  def create
    rsvp = Rsvp.create(adjusted_params)

    if rsvp.valid?
      RsvpMailer.rsvp_email(rsvp).deliver
    end

    respond_with rsvp, location: nil
  end

  private

  def rsvp_params
    params.require(:rsvp).permit(
      :name, :guests, :attending, :pork_orders, :chicken_orders
    )
  end

  def adjusted_params
    new_params = rsvp_params
    new_params['attending'] = new_params['attending'] == 'true' ? true : false
    new_params
  end
end
