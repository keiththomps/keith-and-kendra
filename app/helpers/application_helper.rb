module ApplicationHelper
  def load_angular
    if Rails.env.test? || Rails.env.development?
      javascript_include_tag('angular.min.js') +
        javascript_include_tag('angular-resource.min.js')
    else
      javascript_include_tag('//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js') +
        javascript_include_tag('//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular-resource.min.js')
    end
  end
end
