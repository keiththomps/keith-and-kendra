class RsvpMailer < ActionMailer::Base

  OUR_ADDRESSES = ['me@keiththomps.com', 'kendraliggett@gmail.com']

  default from: "rsvp@keithandkendra.com"

  def rsvp_email(rsvp)
    @rsvp = rsvp
    @stats = RsvpStats.new
    mail to: OUR_ADDRESSES, subject: "#{rsvp.name} has RSVP'd"
  end
end
