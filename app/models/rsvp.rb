class Rsvp < ActiveRecord::Base

  validates :name, presence: true
  validates :guests, presence: true
  validates :attending, inclusion: { in: [true, false] }
  validates :pork_orders, presence: true
  validates :chicken_orders, presence: true

  def save
    unless attending
      self.guests = 0
      self.pork_orders = 0
      self.chicken_orders = 0
    end

    super
  end
end
