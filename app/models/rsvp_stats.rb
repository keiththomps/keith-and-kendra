class RsvpStats
  attr_reader :count, :guests, :yes, :no, :pork, :chicken

  def initialize
    @count = Rsvp.count
    @guests = Rsvp.sum('guests')
    @yes = Rsvp.where(attending: true).count
    @no = Rsvp.where(attending: false).count
    @pork = Rsvp.sum('pork_orders')
    @chicken = Rsvp.sum('chicken_orders')
  end
end
