KeithAndKendra::Application.routes.draw do
  mount JasmineRails::Engine => "/specs" if defined?(JasmineRails)

  root 'rsvp#index'

  namespace :api do
    resources :rsvps, only: :create
  end
end
