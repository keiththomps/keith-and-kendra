class AddStatusAndOrdersToRsvps < ActiveRecord::Migration
  def change
    add_column :rsvps, :attending, :boolean
    add_column :rsvps, :pork_orders, :integer
    add_column :rsvps, :chicken_orders, :integer
  end
end
