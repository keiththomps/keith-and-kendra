require 'rake/testtask'

namespace :test do
  desc 'Run unit tests'
  Rake::TestTask.new('unit') do |t|
    t.libs.push 'lib'
    t.test_files = FileList.new('test/**/*_test.rb')
    t.verbose = true
  end

  desc 'Run the entire test suite (minitest)'
  task :suite => %w[test:unit]
end
