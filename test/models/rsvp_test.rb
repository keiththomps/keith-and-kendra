require_relative '../ar_test_helper'
require_relative '../../app/models/rsvp'

describe Rsvp do
  subject { Rsvp.new }

  describe 'validations' do
    it 'requires a name' do
      subject.valid?.must_equal false
      subject.errors[:name].must_include "can't be blank"
    end

    it 'requires the number of guests' do
      subject.valid?.must_equal false
      subject.errors[:guests].must_include "can't be blank"
    end

    it 'requires attendence status' do
      subject.valid?.must_equal false
      subject.errors[:attending].must_include "is not included in the list"
    end

    it 'requires the number of pork orders' do
      subject.valid?.must_equal false
      subject.errors[:pork_orders].must_include "can't be blank"
    end

    it 'requires the number of chicken orders' do
      subject.valid?.must_equal false
      subject.errors[:chicken_orders].must_include "can't be blank"
    end
  end

  describe 'defaulting on attendence' do
    subject { Rsvp.new(name: 'Keith', guests: 1, attending: false,
                       pork_orders: 1, chicken_orders: 3) }

    it 'defaults the number of guests to zero if attending is false' do
      subject.save
      subject.guests.must_equal 0
    end

    it 'defaults the number of pork orders to zero if attending is false' do
      subject.save
      subject.pork_orders.must_equal 0
    end

    it 'defaults the number of chicken orders to zero if attending is false' do
      subject.save
      subject.chicken_orders.must_equal 0
    end
  end
end
